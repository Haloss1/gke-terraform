# GKE Terraform Scripts

<img src="/uploads/ee44e73909b14de32e810976bd6209e3/Screenshot_20200804_223528.png" width="500">
<img src="/uploads/9c5b60551b93e74f4f5b0076d8dd03d5/Screenshot_2020-08-05_Kubernetes_Dashboard.png" width="560">

#### Requirements:

- [Google Cloud Project](https://cloud.google.com/resource-manager/docs/creating-managing-projects) with [billing](https://cloud.google.com/billing/docs/how-to/modify-project) and the [Cloud Resource API](https://console.developers.google.com/apis/library/cloudresourcemanager.googleapis.com) enabled and the [Service Account Credentials](https://cloud.google.com/iam/docs/service-accounts) with ```Editor``` Access (One project per environment)
- [Terraform](https://www.terraform.io/downloads.html)
- [Terragrunt](https://terragrunt.gruntwork.io/docs/getting-started/install/)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

#### Deployment process:

1. Clone the repo and ```cd``` into the ```gke-terraform/live``` folder (Here is were you find the environment folders).
2. Edit the ```common.hcl``` file with your organization name.
3. Create/Delete folders according to your environment needs, you can use one of the existing environment folders as base (Don't forget to edit the ```env.hcl``` with your environment name!).
4. Provide your [Service Account Credentials](https://cloud.google.com/iam/docs/service-accounts) as a credentials.json file (It is recommended that the contents be supplied by a environment variable by your CI solution and written down to disk on pipeline execution or a secrets manager should be used to decode the files during pipeline run instead of uploading it directly to your repo)
5. Run ```terragrunt plan-all``` from the root of your environment folder (Example: ```gke-terraform/live/development```) to view your current environment infrastructure plan.
5. Run ```terragrunt apply-all``` from the root of your environment folder to create the resources based on your current environment infrastructure plan.
6. Wait until is done, by the end the cluster should be deployed and you would be able to retrieve your ```kubeconfig``` file from the GKE console.

#### File structure:
```
gke-terraform <= Repo folder
├── README.md
├── live <= Environments folder that defines the infrastructure
│   ├── common.hcl <= Common variables that apply across environments
│   ├── development <= development environment folder
│   │   ├── credentials.json <= Development Project's Google Service Account credentials
│   │   ├── env.hcl <= Variables for development environment
│   │   └── gcp-gke
│   │       └── terragrunt.hcl <= Includes env files and supplies values to corresponding module
│   ├── production <= Production environment folder
│   │   ├── credentials.json <= Production Project's Google Service Account credentials
│   │   ├── env.hcl <= Variables for production environment
│   │   └── gcp-gke
│   │       └── terragrunt.hcl <= Includes env files and supplies values to corresponding module
│   └── terragrunt.hcl
└── modules <= Reused by live/
    └── gcp-gke
        ├── main.tf <= Declarations
        ├── outputs.tf <= Module's output variables
        └── variables.tf <= Module's input variables
```